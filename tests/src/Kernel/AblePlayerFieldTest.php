<?php

namespace Drupal\Tests\ableplayer\Kernel;

use Drupal\Tests\field\Kernel\FieldKernelTestBase;

/**
 * Tests the Able Player field install and content.
 */
class AblePlayerFieldTest extends FieldKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'path',
    'file',
    'media',
    'ableplayer',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['field']);
    $this->installConfig(['path']);
    $this->installConfig(['file']);
    $this->installConfig(['media']);
    $this->installConfig(['ableplayer']);

    /*
    FieldStorageConfig::create([
    ])->save();

    FieldConfig::Create(]
    ])->save();
     */
  }

  /**
   * Test field creation.
   */
  public function testFieldCreation() {
    \Drupal::service('module_installer')->install(['ableplayer']);
  }

}
