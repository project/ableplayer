<?php

namespace Drupal\Tests\ableplayer\Kernel\Installer;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the installation of modules.
 *
 * @group Module
 */
class InstallKernelTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'ableplayer',
  ];

  /**
   * Tests installing ableplayer module.
   */
  public function testInstallAblePlayer() {

    // Able Player creates a media type and new fields.
    // Ensure the fields are installed.
    $this->assertTrue(
          \Drupal::service('module_installer')
            ->install(
              [
                'ableplayer',
              ]
          )
      );
  }

}
