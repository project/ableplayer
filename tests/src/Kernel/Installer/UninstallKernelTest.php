<?php

namespace Drupal\Tests\ableplayer\Kernel\Installer;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the uninstallation of modules.
 *
 * @group Module
 */
class UninstallKernelTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'user',
    'field',
    'file',
    'image',
    'media',
    'ableplayer',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();
    $this
      ->installSchema(
          'user', [
            'users_data',
          ]
      );
    $this
      ->installEntitySchema('media');
    $this
      ->installEntitySchema('file');
    $this
      ->installConfig(
          [
            'media',
          ]
      );
  }

  /**
   * Tests uninstalling ableplayer module.
   */
  public function testUninstallAblePlayer() {

    // Able Player creates a media type and new fields.
    // Ensure the fields are uninstalled.
    \Drupal::service('module_installer')
      ->uninstall(
          [
            'ableplayer',
          ]
      );
  }

}
