<?php

namespace Drupal\Tests\ableplayer\Functional;

/**
 * @file
 * Test case for Able Player display settings validation.
 */

/**
 * Test case for Able Player settings validation.
 *
 * Ensure that the settings validation functions operate correctly.
 *
 * @group ableplayer
 */
class AblePlayerValidationTest extends AblePlayerFunctionalTestBase {

  /**
   * Able Player is available as a file display.
   */
  public function testAblePlayerPresent() {
    $assert_session = $this->assertSession();

    $this->drupalGet('admin/structure/media');
    $assert_session->pageTextContains(t('Media types'));
    $assert_session->pageTextContains(t('Able Player Caption'));
    $assert_session->pageTextContains(t('Video'));

    $this->drupalGet("admin/structure/media/manage/video_test");
    $assert_session->pageTextContains(t('Video'));

    $this->drupalGet("admin/structure/media/manage/video_test/fields");
    $assert_session->pageTextContains(t('Ableplayer Media Caption'));
    $assert_session->pageTextContains(t('Chapters'));
    $assert_session->pageTextContains(t('Description'));
    $assert_session->pageTextContains(t('Poster Image'));
    $assert_session->pageTextContains(t('Sign Language Video'));
    $assert_session->pageTextContains(t('Video file'));
  }

}
