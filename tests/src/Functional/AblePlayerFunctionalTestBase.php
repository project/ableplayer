<?php

namespace Drupal\Tests\ableplayer\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;

/**
 * Base test class for Able Player functional tests.
 *
 * Test case for Able Player settings validation. Ensure that the settings
 * validation functions operate correctly.
 *
 * @group ableplayer
 */
abstract class AblePlayerFunctionalTestBase extends BrowserTestBase {

  use MediaTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'ableplayer',
    'ableplayer_test',
    'file',
    'field',
    'field_ui',
    'image',
    'node',
    'media',
    'path',
    'user',
    'system',
  ];

  /**
   * Permissions for the admin user that will be logged in for the test.
   *
   * @var array
   */
  protected static $adminUserPermissions = [
    'access content overview',
    'access media overview',
    'administer content types',
    'administer media display',
    'administer media fields',
    'administer media form display',
    'administer media types',
    'administer media',
    'administer modules',
    'administer node display',
    'administer node fields',
    'administer node form display',
    'bypass node access',
    'view all revisions',
    'view media',
  ];

  /**
   * The admin user that will be logged in for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * The non-admin user that will be used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser(static::$adminUserPermissions);
    $this->user = $this->drupalCreateUser([]);
    $this->drupalLogin($this->adminUser);

    $page = $this->getSession()->getPage();

    $this->container->get('module_installer')->uninstall(['ableplayer'], FALSE);
    $this->drupalGet('/admin/modules');
    $page->checkField('modules[ableplayer][enable]');
    $page->pressButton('Install');
  }

}
