<?php

namespace Drupal\Tests\ableplayer\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * @file
 * Test case for Able Player core functions.
 */

/**
 * Test case for Able Player core functions.
 *
 * @group ableplayer
 */
class AblePlayerCoreTest extends BrowserTestBase {

  /**
   * Module dependencies.
   *
   * @var array
   */
  protected static $modules = [
    'ableplayer',
    'field',
    'file',
    'media',
    'path',
  ];

  /**
   * The Drupal user that will perform the test operations.
   *
   * @var object
   */
  protected $user;

  /**
   * Default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Implementation of BrowserTestBase::setUp().
   */
  public function setUp(): void {
    $user = [
      'access administration pages',
      'administer site configuration',
      'view the administration theme',
    ];

    parent::setUp();

    $this->user = $this->drupalCreateUser($user);
    $this->drupalLogin($this->user);
  }

  /**
   * Test that the theme hook returns the ableplayer key.
   */
  public function testAblePlayerTheme() {
    $existing = [];
    $type = 'module';
    $theme = 'ableplayer';
    $path = \Drupal::service('extension.path.resolver')->getPath('module', 'ableplayer');
    $themes = \Drupal::moduleHandler()->invoke('ableplayer', 'theme', [
      $existing,
      $type,
      $theme,
      $path,
    ]);
    $keys = [
      'ableplayer_video',
      'media__able_player_caption_view_mode',
      'field__field_ableplayer_media_caption',
      'ableplayer_audio',
      'ableplayer_caption',
      'field__ableplayer_caption',
      'ableplayer_chapter',
      'field__ableplayer_chapter',
      'ableplayer_sign_language',
      'field__ableplayer_sign_language',
      'ableplayer_poster_image',
      'field__ableplayer_poster_image',
    ];

    foreach ($keys as $key) {
      $this->assertTrue(array_key_exists($key, $themes), 'Verifying hook_theme entry ' . $key . '.');
    }
  }

}
