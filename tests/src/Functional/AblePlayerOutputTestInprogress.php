<?php

namespace Drupal\Tests\ableplayer\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * @file
 * Test case for Able Player file display output.
 */

/**
 * Test case for Able Player formatter output.
 *
 * Verify the HTML output of Able Player is correct with given settings.
 *
 * @group ableplayer
 */
class AblePlayerOutputTestInprogress extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules dependencies.
   *
   * @var array
   */
  protected static $modules = [
    'ableplayer',
    'file',
    'field',
    'media',
    'path',
  ];

  /**
   * The Drupal user that will perform the test operations.
   *
   * @var object
   */
  protected $user;

  /**
   * Implementation of BrowserTestBase::setUp().
   */
  public function setUp(): void {
    $user = [
      'access administration pages',
      'administer file types',
      'administer files',
      'administer nodes',
      'bypass file access',
      'bypass node access',
      'create files',
      'view the administration theme',
    ];

    parent::setUp($dependencies);

    $this->user = $this->drupalCreateUser($user);
    $this->drupalLogin($this->user);

    $data = [];
    $data['displays[ableplayer][status]'] = 1;
    $data['displays[ableplayer][weight]'] = -50;
    $this->drupalGet('admin/structure/file-types/manage/video/file-display');

    $this->submitForm($data, t('Save configuration'));
    $this->assertSession()->pageTextContains(t('Your settings have been saved.'));

    $data = [];
    $data['displays[ableplayer][status]'] = 1;
    $data['displays[ableplayer][weight]'] = -50;
    $this->drupalGet('admin/structure/file-types/manage/audio/file-display');

    $this->submitForm($data, t('Save configuration'));
    $this->assertSession()->pageTextContains(t('Your settings have been saved.'));
  }

  /**
   * Tests that the Able Player module successfully displays for audio uploads.
   */
  public function testAblePlayerAudioLoad() {
    $data = [];
    $data['files[upload]'] = realpath(\Drupal::service('extension.list.module')->getPath('ableplayer') . '/test/files/sample.ogg');
    $this->drupalGet('file/add');

    $this->submitForm($data, t('Next'));

    $sample_file_path = realpath(\Drupal::service('extension.list.module')->getPath('ableplayer') . '/test/files/sample.vtt');

    $data = [];
    $data['files[ableplayer_caption_und_0]'] = $sample_file_path;
    $data['files[ableplayer_description_und_0]'] = $sample_file_path;
    $data['files[ableplayer_chapters_und_0]'] = $sample_file_path;
    $this->drupalGet('file/1/edit');

    $this->submitForm($data, t('Save'));

    $this->drupalGet('file/1');
    $this->assertSession()->responseMatches('@<audio[^<>]+data-able-player>@', 'Attribute "data-able-player" found in raw HTML.');
    $this->assertSession()->responseContains('ableplayer.min.js');
    $this->assertSession()->responseContains('ableplayer.min.css');
  }

  /**
   * Tests that the Able Player module successfully displays for video uploads.
   */
  public function testAblePlayerVideoLoad() {
    $data = [];
    $data['files[upload]'] = realpath(\Drupal::service('extension.list.module')->getPath('ableplayer') . '/test/files/sample.webm');
    $this->drupalGet('file/add');

    $this->submitForm($data, t('Next'));

    $sample_file_path = realpath(\Drupal::service('extension.list.module')->getPath('ableplayer') . '/test/files/sample.vtt');

    $data = [];
    $data['files[ableplayer_caption_und_0]'] = $sample_file_path;
    $data['files[ableplayer_description_und_0]'] = $sample_file_path;
    $data['files[ableplayer_chapters_und_0]'] = $sample_file_path;
    $this->drupalGet('file/1/edit');

    $this->submitForm($data, t('Save'));

    $this->drupalGet('file/1');
    $this->assertSession()->responseMatches('@<video[^<>]+data-able-player>@', 'Attribute "data-able-player" found in raw HTML.');
    $this->assertSession()->responseMatches('@<track kind="captions" src=".*?" srclang="" label="" />@', 'Caption track found in raw HTML.');
    $this->assertSession()->responseMatches('@<track kind="descriptions" src=".*?" srclang="" label="" />@', 'Description track found in raw HTML.');
    $this->assertSession()->responseMatches('@<track kind="chapters" src=".*?" srclang="" label="" />@', 'Chapters track found in raw HTML.');
    $this->assertSession()->responseContains('ableplayer.min.js');
    $this->assertSession()->responseContains('ableplayer.min.css');
  }

}
