<?php

namespace Drupal\Tests\ableplayer\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;

/**
 * @file
 * Test case for Able Player core functions.
 */

/**
 * Test case for Able Player core functions.
 *
 * @group ableplayer
 */
class AblePlayerInstallTest extends BrowserTestBase {
  use MediaTypeCreationTrait {
    createMediaType as drupalCreateMediaType;
  }

  /**
   * Module dependencies.
   *
   * @var array
   */
  protected static $modules = [
    'ableplayer',
    'field',
    'file',
    'media',
    'path',
  ];

  /**
   * The Drupal user that will perform the test operations.
   *
   * @var object
   */
  protected $user;

  /**
   * Default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Implementation of BrowserTestBase::setUp().
   */
  public function setUp(): void {
    $user = [
      'access administration pages',
      'administer site configuration',
      'view the administration theme',
    ];

    parent::setUp();

    $this->user = $this->drupalCreateUser($user);
    $this->drupalLogin($this->user);
    $this->moduleHandler = $this->container->get('module_handler');
    $this->moduleInstaller = $this->container->get('module_installer');
  }

  /**
   * Test installation.
   */
  public function testInstallation() {
    $this->assertFalse($this->moduleHandler->moduleExists('ableplayer'));
    $this->assertTrue($this->moduleInstaller->install(['ableplayer']));
    $this->reloadServices();
    $this->assertTrue($this->moduleHandler->moduleExists('ableplayer'));
  }

  /**
   * Reload services.
   */
  protected function reloadServices() {
    $this->rebuildContainer();
    $this->moduleHandler = $this->container->get('module_handler');
    $this->moduleInstaller = $this->container->get('module_installer');
  }

}
