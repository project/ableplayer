<?php

namespace Drupal\ableplayer\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Component\Utility\Html;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\media\OEmbed\UrlResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'ableplayer_remote_video' formatter.
 */
#[FieldFormatter(
    id: 'ableplayer_remote_video',
    label: new TranslatableMarkup('Ableplayer Remote Video'),
    description: new TranslatableMarkup('Display the remote file using Ableplayer.'),
    field_types: [
      'link',
      'string',
      'string_long',
    ]
)]
/**
 * Allows Ableplayer to be a media format for video files from oEmbed.
 */
class AbleplayerRemoteVideoFormatter extends FormatterBase {

  /**
   * The oEmbed URL resolver service.
   *
   * @var \Drupal\media\OEmbed\UrlResolverInterface
   */
  protected $urlResolver;

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\media\OEmbed\UrlResolverInterface $url_resolver
   *   The oEmbed URL resolver service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, UrlResolverInterface $url_resolver) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->urlResolver = $url_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $plugin_id,
          $plugin_definition,
          $configuration['field_definition'],
          $configuration['settings'],
          $configuration['label'],
          $configuration['view_mode'],
          $configuration['third_party_settings'],
          $container->get('media.oembed.url_resolver')
      );
  }

  /**
   * {@inheritdoc}
   */
  public static function getMediaType() {
    return 'remote_video';
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $parent = $items->getEntity();

    if ($parent->hasField('ableplayer_audio_description')) {
      $audio_description_url = $parent->ableplayer_audio_description->value;
    }
    else {
      $audio_description_url = NULL;
    }

    foreach ($items as $delta => $item) {
      $main_property = $item->getFieldDefinition()->getFieldStorageDefinition()->getMainPropertyName();
      $value = $item->{$main_property};

      if (empty($value)) {
        continue;
      }

      $provider = $this->urlResolver->getProviderByUrl($value);

      if ($provider->getName() === 'YouTube') {
        $element[$delta] = [
          '#type' => 'html_tag',
          '#tag' => 'video',
          '#attributes' => [
            'data-able-player' => '',
            'data-youtube-id' => $this->getVideoId($provider, $value),
            'data-youtube-desc-id' => $this->getVideoId($provider, $audio_description_url),
          ],
          '#attached' => [
            'library' => [
              'ableplayer/ableplayer',
            ],
          ],
        ];
      }
      elseif ($provider->getName() === 'Vimeo') {
        $element[$delta] = [
          '#type' => 'html_tag',
          '#tag' => 'video',
          '#attributes' => [
            'id' => Html::getUniqueId("vimeo"),
            'data-able-player' => '',
            'data-vimeo-id' => $this->getVideoId($provider, $value),
            'data-vimeo-desc-id' => $this->getVideoId($provider, $audio_description_url),
          ],
          '#attached' => [
            'library' => [
              'ableplayer/ableplayer',
              'ableplayer/ableplayer-vimeo',
            ],
          ],
        ];
      }
    }
    return $element;
  }

  /**
   * Retrieve the ID of the remote video based on provider.
   */
  private function getVideoId($provider, $url) {
    // Set $id to be an empty string to prevent an unset variable warning if an
    // audio description URL is not also supplied.
    $id = '';
    if (is_null($url)) {
      return $id;
    }

    if ($provider->getName() === 'YouTube') {
      $scheme = 'https://*.youtube.com/watch*';
      $regexp = str_replace(['.', '*'], ['\.', '.*'], $scheme);
      if (preg_match("|^$regexp$|", $url)) {
        $parts = UrlHelper::parse($url);
        $id = $parts['query']['v'];
      }
      // Currently YouTube returns a 404 for this pattern so this code is
      // never called.
      $scheme = 'https://*.youtube.com/v/*';
      $regexp = str_replace(['.', '*'], ['\.', '.*'], $scheme);
      if (preg_match("|^$regexp$|", $url)) {
        $parts = UrlHelper::parse($url);
        $path = explode('/', $parts);
        $id = $path[2];
      }

      $scheme = 'https://youtu.be/*';
      $regexp = str_replace(['.', '*'], ['\.', '.*'], $scheme);
      if (preg_match("|^$regexp$|", $url)) {
        $parts = parse_url($url, PHP_URL_PATH);
        $path = explode('/', $parts);
        $id = $path[1];
      }
    }

    if ($provider->getName() === 'Vimeo') {

      // The /channels and /ondemand do not return playable video for
      // Ableplayer.
      $schemes = [
        "https://vimeo.com/*",
        "https://vimeo.com/album/*/video/*",
        "https://vimeo.com/channels/*/*",
        "https://vimeo.com/groups/*/videos/*",
        "https://vimeo.com/ondemand/*/*",
        "https://player.vimeo.com/video/*",
      ];
      foreach ($schemes as $scheme) {
        $regexp = str_replace(['.', '*'], ['\.', '.*'], $scheme);
        if (preg_match("|^$regexp$|", $url)) {
          $parts = parse_url($url, PHP_URL_PATH);
          $path = explode('/', $parts);
          // If the Vimeo URL has more than two parts, Able Player wants the
          // full URL, not just the ID. We check the length of the URL path
          // and if it has more than two parts, assemble the Vimeo URL.
          if (count($path) > 2) {
            $url_part_one = array_pop($path);
            $url_part_two = array_pop($path);
            $id = "https://player.vimeo.com/video/" . $url_part_two . "?h=" . $url_part_one;
          }
          else {
            // Grab just the last part of the URL, which is the ID.
            $id = array_pop($path);
          }
          break;
        }
      }
    }
    return $id;
  }

}
