<?php

namespace Drupal\ableplayer\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\Plugin\Field\FieldFormatter\FileMediaFormatterBase;

/**
 * Plugin implementation of the 'ableplayer_video' formatter.
 */
#[FieldFormatter(
    id: 'ableplayer_video',
    label: new TranslatableMarkup('Ableplayer Video'),
    description: new TranslatableMarkup('Display the file using Ableplayer.'),
    field_types: [
      'file',
    ]
)]
/**
 * Configures the output needed for the Ableplayer HTML container.
 */
class AbleplayerVideoFormatter extends FileMediaFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function getMediaType() {
    return 'video';
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'controls' => FALSE,
      'autoplay' => FALSE,
      'loop' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    $parent = $items->getEntity();

    if ($parent->hasField('field_ableplayer_media_caption')) {
      foreach ($elements as &$element) {
        $element['#caption'] = $parent->field_ableplayer_media_caption->view(
          [
            'type' => 'entity_reference_entity_view',
            'settings' => ['view_mode' => 'able_player_caption_view_mode'],
          ]);
        // $element['#caption'] = $parent->field_ableplayer_media_caption->view(
        // ['type' => 'entity_reference_label']);
      }
    }

    if ($parent->hasField('ableplayer_chapter')) {
      foreach ($elements as &$element) {
        $element['#chapter'] = $parent->ableplayer_chapter->view(['type' => 'ableplayer_chapter']);
      }
    }

    if ($parent->hasField('ableplayer_poster_image')) {
      foreach ($elements as &$element) {
        $poster_array = $parent->ableplayer_poster_image->view(['type' => 'ableplayer_poster_image']);
        $poster = \Drupal::service('renderer')->render($poster_array);
        $element['#attributes']->setAttribute('poster', $poster);
      }
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSourceFiles(EntityReferenceFieldItemListInterface $items, $langcode) {
    $source_files = parent::getSourceFiles($items, $langcode);
    $parent = $items->getEntity();
    $data_sign_src_array = $parent->ableplayer_sign_language->view(['type' => 'ableplayer_sign_language']);
    $data_sign_src = \Drupal::service('renderer')->render($data_sign_src_array);

    foreach ($source_files as $source_file) {
      foreach ($source_file as $element) {
        $element['source_attributes']->setAttribute('data-sign-src', $data_sign_src);
      }
    }

    return $source_files;
  }

}
