<?php

namespace Drupal\ableplayer\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\Plugin\Field\FieldFormatter\FileMediaFormatterBase;

/**
 * Plugin implementation of the 'ableplayer_audio' formatter.
 */
#[FieldFormatter(
    id: 'ableplayer_audio',
    label: new TranslatableMarkup('Ableplayer Audio'),
    description: new TranslatableMarkup('Display the file using Ableplayer.'),
    field_types: [
      'file',
    ]
)]
/**
 * Setup audio media type display settings and configure Ableplayer.
 */
class AbleplayerAudioFormatter extends FileMediaFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function getMediaType() {
    return 'audio';
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'controls' => TRUE,
      'autoplay' => FALSE,
      'loop' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    $parent = $items->getEntity();

    if ($parent->hasField('field_ableplayer_media_caption')) {
      foreach ($elements as &$element) {
        $element['#caption'] = $parent->field_ableplayer_media_caption->view(
        [
          'type' => 'entity_reference_entity_view',
          'settings' => ['view_mode' => 'able_player_caption_view_mode'],
        ]);
      }
    }

    return $elements;
  }

}
