<?php

namespace Drupal\ableplayer\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\Attribute;

use Drupal\file\Plugin\Field\FieldFormatter\FileMediaFormatterBase;

/**
 * Plugin implementation of the 'ableplayer_caption' formatter.
 */
#[FieldFormatter(
    id: 'ableplayer_caption',
    label: new TranslatableMarkup('Ableplayer Media Caption'),
    description: new TranslatableMarkup('Display media caption'),
    field_types: [
      'file',
    ]
)]
/**
 * Retrieve file needed to display captions and configure display settings.
 */
class AbleplayerCaptionFormatter extends FileMediaFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function getMediaType() {
    return 'file';
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'controls' => TRUE,
      'autoplay' => FALSE,
      'loop' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  protected function getSourceFiles(EntityReferenceFieldItemListInterface $items, $langcode) {
    $source_files = parent::getSourceFiles($items, $langcode);
    $parent = $items->getEntity();

    foreach ($parent->getTranslationLanguages() as $langcode => $language) {
      $translation = $parent->getTranslation($langcode);
      $source_attributes = new Attribute();

      $source_attributes
        ->setAttribute('src', \Drupal::service('file_url_generator')->generateAbsoluteString($translation->ableplayer_caption->entity->GetFileUri()))
        ->setAttribute('srclang', $langcode)
        ->setAttribute('label', $language->getName());

      $source_files[] = [
        [
          'file' => $translation,
          'source_attributes' => $source_attributes,
        ],
      ];
    }
    return $source_files;
  }

}
