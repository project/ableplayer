## Introduction

**[Able Player](https://github.com/ableplayer/ableplayer)** is a fully
accessible cross-browser media player created by accessibility specialist
Terrill Thompson. It uses the HTML5 `<audio>` or `<video>` element for browsers that support them.

Details on the usage of the Able Player library, including up-to-date support
for filetypes and third-party media hosts, may be found at the [**Able Player
  GitHub page**](https://github.com/ableplayer/ableplayer).


#### Requirements

This module requires no modules outside of Drupal core.

This module requires the Drupal core Media module and if you wish to enable multiple caption files to support additional languages, you will need to enable the translation core module.


### Installation

  1. Download and enable the module using Composer, Drush, or file upload.
  2. Enable the module either either through the Drupal admin
     (Extend › Media › Ableplayer) or Drush (`drush pm-enable ableplayer`).


### Configuration
In Able Player 3.x, local video configuration is handled during installation. If you are coming from Drupal 8.x, you will need to follow steps 1 and 2.
  1. If necessary, create a Video Media type, Structure › Media › Add (`admin/structure/media/add`) and select Video file as the media source.
  2. Set the video file format to Ableplayer from Structure › Media Types › Video
     › Manage display (`/admin/structure/media/manage/videos/display`) under the display column. Change this display to use Ableplayer.

#### Remote Video
If you're using remote video (YouTube or Vimeo), configure Able Player to be used by going to Structure › Media Types › Remote Video › Manage display('admin/structure/media/manage/remote_video/display') and choose Able Player Remote Video as the format.


### Usage

  1. Navigate to Content › Media and select the 'Add media' button.
  2. Upload your video file and associated captions, descriptions, chapters, a poster image, and a sign language video, if applicable.
  3. Attach the video to content using the desired method (Media Browser, entity reference, etc.)
  6. The video will be rendered with the Able Player wrapper and controls. The output
     should be similar to that found in the [**Able Player examples**](https://ableplayer.github.io/ableplayer/demos/video1.html).

#### Additional Language Captions
You must enable the translation core module and add the languages you wish to support at site-url/admin/config/regional/language.

  1. Navigate to Content > Media and select the 'Add media' button.
  2. Select Able Player Caption
  3. Upload your primary language caption file and save the entity
  4. Select the caption you just added and then choose the translate option
  5. Upload your next language and save the entity

#### Reinstalling the Module
If you have uninstalled the module and choose to reinstall it at a later point in time you need to go to /admin/structure/media/manage/video/display, and submit the form to ensure the view mode is again used for local videos.

## 2.0.0-beta2 versus 3.x
The 2.x branch was developed to support a wide variety of features that come packaged as part of Able Player. Support for captions in multiple languages was something 2.x did not consider. This was [rightfully requested](https://www.drupal.org/project/ableplayer/issues/3214408) shortly after the 2.x release. To make that work following Drupal patterns we chose to support it through the translation module.

This requires us to bump the version to 3.x as there is no upgrade path for caption files stored on the media entity to be a caption-type entity. If you want to upgrade, we believe it is possible to do so and then reassign the caption files uploaded as Able Player caption entities following the instructions in Additional Language Captions. **Please test this process in a development environment and not on your live site**.
